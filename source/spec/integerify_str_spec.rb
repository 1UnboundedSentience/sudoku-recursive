require_relative '../sudoku.rb'


describe '#integerify_str'  do

  let(:test_board) { Sudoku.new("1-58-2----9--764-52--4--819-19--73-6762-83-9-----61-5---76---3-43--2-5-16--3-89--") }

  it 'should convert a 2D array of strings to integers with dashes converted to 0' do
    expect(test_board.integerify_str).to eq(
      [[1, 0, 5, 8, 0, 2, 0, 0, 0],
       [0, 9, 0, 0, 7, 6, 4, 0, 5],
       [2, 0, 0, 4, 0, 0, 8, 1, 9],
       [0, 1, 9, 0, 0, 7, 3, 0, 6],
       [7, 6, 2, 0, 8, 3, 0, 9, 0],
       [0, 0, 0, 0, 6, 1, 0, 5, 0],
       [0, 0, 7, 6, 0, 0, 0, 3, 0],
       [4, 3, 0, 0, 2, 0, 5, 0, 1],
       [6, 0, 0, 3, 0, 8, 9, 0, 0]])

  end
end