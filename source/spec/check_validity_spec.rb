require_relative '../sudoku.rb'

describe "check_validity" do
    it 'should check nested array and return true' do
    expect(check_validity([[5, 3, 4, 6, 7, 8, 9, 1, 2],[6, 7, 2, 1, 9, 5, 3, 4, 8]])).to be_truthy
  end

  it 'should check nested array and return false' do
    expect(check_validity([[5, 3, 4, 6, 7, 8, 9, 1, 2],[6, 7, 2, 1, 0, 5, 3, 4, 8]])).to be_falsey
  end
end
