class Sudoku
  attr_accessor :board_string

  def initialize(board_string)
    @board_string = board_string
    @given_number_coords = find_given_number_chords
  end

  def solve
  end

  def board
  end

  # Returns a string representing the current state of the boar

  def find_given_number_chords
    output = []
    formatted_for_check_integer_board.each_with_index do |number, index|
      if number > 0
        output << index
      end
    end
    output
  end

  def to_s
  end

  def format_board
    board_string.scan(/.{9}/).map { |n| n.split("") }
  end

  def display_board(board = format_board)
    board.each do |row|
      puts row.join(" ")
    end
  end
end


# Check validity of sudoku board
class Sudoku

   def complete_checker(board)
      output =[]
      board.each do |number|
        return false if number == 0
        output << number
      end
      output.reduce(:+) == 405
   end

end


class Sudoku

  def integerify_str (board = format_board)
     integer_board = Array.new(9) {Array.new()}
     board.each_with_index { |row_of_nine, row_index|
       row_of_nine.each {|cell| integer_board[row_index] << cell.to_i
       }
     }
    return integer_board
  end




  def convert_columns_to_rows(board = format_board)
    output_board = Array.new(9) { Array.new(9) }
    board.length.times do |y|
      board.length.times do |x|
        output_board[x][y] = board[y][x]
      end
    end
    output_board
  end
end

# Convert Boxes to rows
class Sudoku

  def display_boxes(board = format_board)
    [ #Upper boxes
      surrounding_coordinates_associated_values(board, [1,1]),
      surrounding_coordinates_associated_values(board, [1,4]),
      surrounding_coordinates_associated_values(board, [1,7]),
      #Middle Boxes
      surrounding_coordinates_associated_values(board, [4,1]),
      surrounding_coordinates_associated_values(board, [4,4]),
      surrounding_coordinates_associated_values(board, [4,7]),
      #Bottom Boxes
      surrounding_coordinates_associated_values(board, [7,1]),
      surrounding_coordinates_associated_values(board, [7,4]),
      surrounding_coordinates_associated_values(board, [7,7]),
    ]
  end

  def xy_lookup(two_dimensional_array, coordinates)
    row = coordinates[0]
    column = coordinates[1]
    two_dimensional_array[row][column]
  end

  def surrounding_coordinates_associated_values(two_dimensional_array, coordinates)
      row = coordinates[0]
      column = coordinates[1]

      coordinates = [
        [row, column],          #center
        [(row - 1), (column)],  # up
        [(row + 1), (column)],  # down
        [(row), (column - 1)],  # left
        [(row), (column + 1)],  # right

        [(row - 1), (column - 1)], #upleft
        [(row - 1), (column + 1)], #upright
        [(row + 1), (column - 1)], #downleft
        [(row + 1), (column + 1)], #downright
      ]

      values = []
      coordinates.each do |xyvalues|
        unless xyvalues.any? { |n| n < 0 || n > two_dimensional_array.length-1 }
          values << xy_lookup(two_dimensional_array, xyvalues)
          values
        end
      end

      values
  end


end

# LOGIC OF RECURSIVE SUDOKU
class Sudoku

  def formatted_for_check_integer_board
    board_string.split("").map(&:to_i).flatten(1)
  end

  def sudoku_recursive_solver(board = formatted_for_check_integer_board, current_index = 0)

    new_board = board.dup
    sudoku_number = 1
    if new_board[current_index] > 0
      sudoku_recursive_solver(new_board, current_index += 1)
    end

    while sudoku_number < 10

      forward_check = 1
      while @given_number_coords.include?(current_index + forward_check)
        forward_check += 1
      end

      system('clear')
      display_board(one_d_to_two_d(new_board))

      new_board[current_index] = sudoku_number
      if wrapper_checker(new_board) == true # is it a valid sudoku number
        return true if complete_checker(new_board) == true
        return true if sudoku_recursive_solver(new_board, current_index += forward_check)
      end
      sudoku_number += 1
    end


    false
  end

  def wrapper_checker(board)
    return false if !row_checker(one_d_to_two_d(board))
    return false if !row_checker(convert_columns_to_rows(one_d_to_two_d(board)))
    return false if !row_checker(display_boxes(one_d_to_two_d(board)))
    true
  end

  def row_checker(board)
    board.each do |row|
      no_zeros = row.dup
      no_zeros.delete(0)
      return false if no_zeros.uniq != no_zeros
    end
    true
  end


  def one_d_to_two_d(one_d)
    one_d.each_slice(9).to_a
  end

end

